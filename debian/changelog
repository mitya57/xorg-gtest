xorg-gtest (0.7.1-5) unstable; urgency=medium

  * debian/rules:
    + Ignore unit test failures. They don't appear in local sbuild runs.
      Only on Debian buildd infrastructure we see them. (Closes: #902525).
    + Use dh_missing override for --fail-missing check.
  * debian/control:
    + Bump Standards-Version: to 4.3.0. No changes needed.
  * debian/{control,compat}:
    + Drop compat file. Switch to debhelper-compat notation in control file.
      Bump to DH compat level version 12.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 04 Feb 2019 12:17:12 +0100

xorg-gtest (0.7.1-4) unstable; urgency=medium

  * debian/control:
    + Update Vcs-*: fields. Packaging Git has been migrated to salsa.debian.org.
    + Bump Standards-Version: to 4.2.0. No changes needed.
  * debian/patches:
    + Cherry-pick all latest patches from upstream:
      0002-xserver-improve-error-code-reporting.patch
      0003-doc-update-doxygen-file.patch
      0004-doc-remove-DOT_FONTNAME-from-doxygen.in.patch
      0005-Properly-escape-the-D-string-defines.patch.
  * debian/rules:
    + Move xorg-gtest.pc to arch-indep path (/usr/share/pkgconfig/) after
      dh_install.
  * debian/copyright:
    + Use secure URL in Source: field.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 22 Aug 2018 13:07:58 +0200

xorg-gtest (0.7.1-3) unstable; urgency=medium

  * debian/patches:
    + Add sigunused_removed_from_glib_2_26.patch. (Closes: #891735).
  * debian/control:
    + Bump Standards-Version: to 4.1.4.
    + Priority extra -> optional.
  * debian/copyright:
    + Use secure URI to obtain copyright format reference.
  * debian/watch:
    + Use secure URL to obtain upstream sources.
  * debian/libxorg-gtest-dev.examples:
    + Add *.cpp from upstream's examples folder as examples.
  * debian/{control,compat}:
    + Bump to DH compat level 11. Also drop dh-autoreconf from B-D.
  * debian/rules:
    + Drop unnecessary --parallel option from debhelper call.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 24 Apr 2018 12:00:50 +0200

xorg-gtest (0.7.1-2) unstable; urgency=medium

  * Re-upload to unstable unchanged.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 23 Aug 2017 23:34:54 +0200

xorg-gtest (0.7.1-1) experimental; urgency=medium

  * Initial upload to Debian. (Closes: #699403).
  * debian/*: Debianize Ubuntu package (Maintainer: field, Uploaders: field,
    Vcs-*: fields, etc.).

  * debian/watch:
    + Update upstream URL.
  * debian/control:
    + Add get-orig-source rule.
    + Avoid FTBFS on well-known architecture that do not have valgrind
      available.
  * debian/rules:
    + Don't run unit tests in parallel. We saw build failures hinting to a
      not-yet-started Xvfb instance.
    + Use doc's package folder as --docdir in configure run.
  * debian/{control,rules}:
    + Avoid duplicate files in doxygen generated docs.
  * debian/copyright:
    + Add auto-generated copyright.in file.
    + Rewrite from scratch. Ubuntu had GPL-3+ everywhere whereas upstream has
      MIT/X11 in most places. Mention files individually.
  * debian/README.source:
    + Drop. Quilt packaging should not require a README anymore nowadays.
  * debian/*.doc-base:
    + Add API documentation to doc-base.
  * debian/patches:
    + Mention our patch naming scheme in a README file.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 29 May 2017 13:29:34 +0200

xorg-gtest (0.7.1-0ubuntu1) saucy; urgency=low

  * New upstream release
  * Drop upstreamed patches.
    - 0001-fix-example-failures.patch
    - 0002-fix-valgrind-test-failure.patch
    - 0003-remove-timeout-truncation-warning.patch
  * Add upstream patch.
    - 0001-device-add-HasEvent-and-GetAbsData.patch

 -- Maarten Lankhorst <maarten.lankhorst@ubuntu.com>  Tue, 16 Jul 2013 10:40:34 +0200

xorg-gtest (0.7.0-0ubuntu5) raring; urgency=low

  * debian/patches/gtest_extra_ldflags.patch: push extra LDFLAGS required to
    build against gtest / X11.

 -- Mathieu Trudel-Lapierre <mathieu-tl@ubuntu.com>  Mon, 04 Mar 2013 16:25:12 -0500

xorg-gtest (0.7.0-0ubuntu4) raring; urgency=low

  * debian/patches/va_list_usage_armhf.patch: Fix FTBFS on armhf: you can't
    compare va_list to NULL directly due to its implementation on arm.
    (LP: #1136306)

 -- Mathieu Trudel-Lapierre <mathieu-tl@ubuntu.com>  Thu, 28 Feb 2013 15:27:13 -0500

xorg-gtest (0.7.0-0ubuntu3) raring; urgency=low

  * debian/patches/0003-remove-timeout-truncation-warning.patch:
    Elimiate compile-time warnings that cause problems when -Werror is enabled
    (lp: #1110728).

 -- Stephen M. Webb <stephen.webb@ubuntu.com>  Wed, 30 Jan 2013 17:03:28 -0500

xorg-gtest (0.7.0-0ubuntu2) raring; urgency=low

  * debian/libxorg-test-dev.install: only install files from usr/include/xorg,
    so as not to try to install files already owned by libgtest-dev (esp. since
    we Depends on libgtest-dev anyway...)
  * debian/rules: explicitly remove usr/include/gtest from the install; it's
    provided by libgtest-dev. (LP: #1102478)

 -- Mathieu Trudel-Lapierre <mathieu-tl@ubuntu.com>  Mon, 21 Jan 2013 11:43:24 -0500

xorg-gtest (0.7.0-0ubuntu1) raring; urgency=low

  * New upstream release (lp:  #1100382).
  * debian/control: added valgrind as a build dependency (for tests).
  * debian/source/format: changed to 3.0 (quilt)
  * fixed 'make check' target so it does not fail in Ubuntu
  * debian/control (Standards-Version): updated to 3.9.4 (no changes required)
  * debian/copyright: updated

 -- Stephen M. Webb <stephen.webb@ubuntu.com>  Fri, 18 Jan 2013 16:11:32 -0500

xorg-gtest (0.3.0-0ubuntu2) quantal; urgency=low

  * debian/control: Depends on xserver-xorg-video-dummy, it seems required
    (without it ido would bail on in tests)

 -- Sebastien Bacher <seb128@ubuntu.com>  Wed, 22 Aug 2012 11:18:40 +0200

xorg-gtest (0.3.0-0ubuntu1) quantal; urgency=low

  * Ship usr/share/X11/xorg.conf.d/99-virtual-test-devices.conf
  * Update to version 0.3.0
    - Prevent other X servers from grabbing test devices
    - Kill dummy server if test program dies
    - Provide a full X server layout to ensure the dummy video driver is used
    - Wait for dummy server to shut down before exiting
    - Namespace header filenames to prevent clashes with other headers
    - Better logging and error handling
    - Perform checks to see if dummy server could start before attempting
    - Enable the highest verbosity level for the dummy server log file

 -- Chase Douglas <chase.douglas@ubuntu.com>  Fri, 08 Jun 2012 12:39:52 -0700

xorg-gtest (0.2.0-0ubuntu2) precise; urgency=low

  * Make libxorg-gtest-dev depend on xutils-dev >= 1:7.7~1 (LP: #960528)

 -- Chase Douglas <chase.douglas@ubuntu.com>  Tue, 20 Mar 2012 13:00:53 -0700

xorg-gtest (0.2.0-0ubuntu1) precise; urgency=low

  * Update to xorg-gtest 0.2.0
    - Remove precompiled libraries and
    - Update for source-only distribution (LP: #959836)

 -- Chase Douglas <chase.douglas@ubuntu.com>  Mon, 19 Mar 2012 18:39:04 -0700

xorg-gtest (0.1.1-0ubuntu2) precise; urgency=low

  * Really fix per-arch symbol for amd64 and powerpc

 -- Chase Douglas <chase.douglas@ubuntu.com>  Fri, 24 Feb 2012 14:18:50 -0800

xorg-gtest (0.1.1-0ubuntu1) precise; urgency=low

  * Update debian/copyright with new upstream location
  * Bump build-depends to debhelper 9, now that it's released
  * Add README.Debian to document dependency of libxorg-gtest0 on libgtest-dev
  * New release 0.1.1 (bug fixes only)
  * Use per-arch symbol listing for method with va_list argument

 -- Chase Douglas <chase.douglas@ubuntu.com>  Fri, 24 Feb 2012 09:44:47 -0800

xorg-gtest (0.1.0-0ubuntu1) precise; urgency=low

  [ Daniel d'Andrada ]
  * Initial release (LP: #932848)

  [ Chase Douglas ]
  * Fix linking against X server libs
    - Added 0001-Fix-linking-against-X-server-libs.patch

 -- Chase Douglas <chase.douglas@ubuntu.com>  Wed, 15 Feb 2012 09:18:17 -0800
